package model;

import java.util.ArrayList;

import java.io.Serializable;

public class Restaurant implements Serializable {
	String yelpId;
	String name;
	String address;
	double distance;
	String phone;
	String yelpUrl;

	ArrayList<Photo> photos;

	public Restaurant(String yelpId, String name, String address,
			double distance, ArrayList<Photo> photos, String phone,
			String yelpUrl) {
		super();
		this.yelpId = yelpId;
		this.name = name;
		this.address = address;
		this.distance = distance;
		this.photos = photos != null ? photos : new ArrayList<Photo>();
		this.phone = phone;
		this.yelpUrl = yelpUrl;
	}

	public String toString() {
		return "name " + name + "\n" + "address " + address + "\n"
				+ "distance " + distance + "\n" + "phone " + phone + "\n";
	}

	public String getYelpId() {
		return yelpId;
	}

	public void setYelpId(String yelpId) {
		this.yelpId = yelpId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddr() {
		return address;
	}

	public void setAddr(String address) {
		this.address = address;
	}

	public double getDistance() {
		return distance;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}

	public ArrayList<Photo> getPhotos() {
		return photos;
	}

	public void setPhotos(ArrayList<Photo> photos) {
		this.photos = photos;
	}

	public String getYelpUrl() {
		return yelpUrl;
	}

	public void setYelpUrl(String url) {
		yelpUrl = url;
	}
}
