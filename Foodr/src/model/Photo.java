package model;

import java.net.URL;
import java.util.ArrayList;
import java.util.Date;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.Serializable;

public class Photo implements Serializable {
	URL url;
	String yelpId;
	Date lastViewd;

	public Photo(URL url, String yelpId, Date lastViewd) {
		super();
		this.url = url;
		this.yelpId = yelpId;
		this.lastViewd = lastViewd;
	}

	public URL getUrl() {
		return url;
	}

	public void setUrl(URL url) {
		this.url = url;
	}

	public String getYelpId() {
		return yelpId;
	}

	public void setYelpId(String yelpId) {
		this.yelpId = yelpId;
	}

	public Date getLastViewd() {
		return lastViewd;
	}

	public void setLastViewd(Date lastViewd) {
		this.lastViewd = lastViewd;
	}

	public static void updateRestaurantPhotos(Restaurant r) {
		r.setPhotos(new ArrayList<Photo>());
		String name = r.getName();
		name = name.replace(" ", "-");
		name = name.substring(0, Math.min(7, name.length()));
		String url = r.yelpUrl;
		url = url.replace('�', 'e');
		url = url.replace("biz", "biz_photos");
		try {
			Document doc = Jsoup.connect(url).userAgent("Mozilla").get();
			Elements photos = doc.select(".photos");
			photos = photos.first().children();
			for (int i = 0; i < Math.min(10, photos.size()); i++) {
				Element photo = photos.get(i);
				String photoUrl = photo.getAllElements().select("img").first()
						.absUrl("src");
				photoUrl = photoUrl.replace("ms.jpg", "l.jpg");
				System.out.println(photoUrl);
				Photo p = new Photo(new URL(photoUrl), r.yelpId, new Date(0));
				r.getPhotos().add(p);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
