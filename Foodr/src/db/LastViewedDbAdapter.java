package db;

import android.content.ContentValues;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class LastViewedDbAdapter extends DbAdapter {

	public static final String LAST_VIEWED_TABLE = "last_viewed";

	public static final String KEY_YELP_ID = "yelp_id";
	public static final int KEY_YELP_ID_NUM = 0;
	public static final String KEY_LAST_SEEN = "last_seen";
	public static final int KEY_LAST_SEEN_NUM = 1;

	public LastViewedDbAdapter(Context context) {
		super(context);
	}

	/**
	 * Table creation sql statement
	 */
	public static final String LAST_VIEWED_CREATE = "create table "
			+ LAST_VIEWED_TABLE + " (" + KEY_YELP_ID + " integer primary key, "
			+ KEY_LAST_SEEN + " text not null" + ");";

	/**
	 * Inserts a restaurant with the given parameters. If the restaurant already
	 * exists in the database, replaces the information.
	 * 
	 * @param restaurantId
	 * @param timeStamp
	 * @return
	 */
	public int insertRestaurant(int restaurantId, String timeStamp) {
		ContentValues vals = new ContentValues();
		vals.put(KEY_YELP_ID, restaurantId);
		vals.put(KEY_LAST_SEEN, timeStamp);
		return (int) mDb.insertWithOnConflict(LAST_VIEWED_TABLE, null, vals,
				SQLiteDatabase.CONFLICT_REPLACE);
	}

	/**
	 * Gets the last seen time of the restaurant.
	 * 
	 * @param restaurantId
	 * @return
	 * @throws SQLException
	 */
	public String getLastSeen(int restaurantId) throws SQLException {
		boolean found = false;
		Cursor mCursor = mDb.query(true, LAST_VIEWED_TABLE, null, KEY_YELP_ID
				+ "=" + restaurantId, null, null, null, null, null);

		if (mCursor != null)
			found = mCursor.moveToFirst();

		if (!found && mCursor != null) {
			mCursor.close();
			return null;
		}

		if (mCursor != null) {
			String lastSeen = mCursor.getString(KEY_LAST_SEEN_NUM);
			mCursor.close();
			return lastSeen;
		}

		return null;
	}
}
