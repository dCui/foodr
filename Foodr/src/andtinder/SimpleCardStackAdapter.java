package andtinder;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import hack.mit.foodr.R;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public final class SimpleCardStackAdapter extends CardStackAdapter {

	public SimpleCardStackAdapter(Context mContext) {
		super(mContext);
	}

	@Override
	public View getCardView(int position, final CardModel model, View convertView,
			ViewGroup parent) {
		if (convertView == null) {
			LayoutInflater inflater = LayoutInflater.from(getContext());
			convertView = inflater.inflate(R.layout.std_card_inner, parent,
					false);
			assert convertView != null;
		}
		
		final View fConvertView = convertView;
		convertView.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				if (model.getOnClickListener() != null)
					model.getOnClickListener().OnClickListener();
				fConvertView.findViewById(R.id.card_overlay).setAlpha(0);
			}});
				
		((ImageView) convertView.findViewById(R.id.image)).setImageDrawable(model.getCardImageDrawable());
		((TextView) convertView.findViewById(R.id.title)).setText(model.getTitle());

		return convertView;
	}

	class DownloadImage extends AsyncTask<Object, Object, Drawable> {

		private ImageView imageView;
		private URL url;

		@Override
		protected Drawable doInBackground(Object... params) {
			imageView = (ImageView) params[0];
			url = (URL) params[1];

			try {
				return drawableFromUrl(url);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Drawable result) {
			if (result != null) {
				imageView.setImageDrawable(result);
			}
		}
	}

	public static Drawable drawableFromUrl(URL url) throws IOException {
		Bitmap x;

		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.connect();
		InputStream input = connection.getInputStream();

		x = BitmapFactory.decodeStream(input);
		return new BitmapDrawable(x);
	}
}
