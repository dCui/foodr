package hack.mit.foodr;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import model.Photo;
import model.Restaurant;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.os.Build;
import andtinder.CardContainer;
import andtinder.CardModel;
import andtinder.SimpleCardStackAdapter;

public class CardActivity extends FragmentActivity {
	public static final String KEY_RESTAURANTS_ARRAY = "key_restaurants_array";
	private static Restaurant[] TEST_RESTAURANT = new Restaurant[3];

	private ArrayList<Restaurant> restaurants;

	private SimpleCardStackAdapter mAdapter;
	private CardContainer mCardContainer;
	private int mPhotoLoadedIndex;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Remove title bar
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);

		setContentView(computeViews());

		Bundle extras = getIntent().getExtras();

		if (extras != null) {
			// restaurants = (Restaurant[]) extras.get(KEY_RESTAURANTS_ARRAY);
			Object[] objArray = (Object[]) extras.get(KEY_RESTAURANTS_ARRAY);

			restaurants = new ArrayList<Restaurant>();

			// Filter out restaurants with no photos
			for (Object restaurant : objArray) {
				Restaurant r = (Restaurant) restaurant;
				if (r.getPhotos() != null && r.getPhotos().size() > 0)
					restaurants.add(r);
			}
		}

		mAdapter = new SimpleCardStackAdapter(this);

		for (int i = 0; i < restaurants.size(); i++) {
			Restaurant r = restaurants.get(i);
			final CardModel c = new CardModel(r.getName(), r.getPhotos().get(0)
					.getUrl());
			/*
			 * c.setOnCardDimissedListener(new
			 * CardModel.OnCardDimissedListener() {
			 * 
			 * @Override public void onLike() { mAdapter.pop(); loadNextPhoto();
			 * }
			 * 
			 * @Override public void onDislike() { mAdapter.pop();
			 * loadNextPhoto(); } });
			 */
			
			final String yelpUrl = r.getYelpId();
			c.setOnClickListener(new CardModel.OnClickListener(){
				@Override
				public void OnClickListener() {
					String uriString = "http://www.yelp.com/biz/" + yelpUrl; 
					startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(uriString))); 
				}
			});

			mAdapter.add(c);
		}

		/*
		 * mPhotoLoadedIndex = 0; while (mPhotoLoadedIndex < restaurants.size()
		 * && mPhotoLoadedIndex < 5) { loadNextPhoto(); }
		 */
		mCardContainer.setAdapter(mAdapter);
	}

	private RelativeLayout computeViews() {
		// Get conversion rates
		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);
		float logicalDensity = metrics.density;

		// Get screen size
		Display display = getWindowManager().getDefaultDisplay();
		int width = display.getWidth(); // deprecated
		int height = display.getHeight(); // deprecated;

		// Content view
		RelativeLayout contentView = new RelativeLayout(this);

		// Title layout
		int bgColor = 0xffd25951;
		int titleLayoutHeight = (int) Math.ceil(0.1 * width * logicalDensity);
		int titleLayoutId = 0;
		RelativeLayout titleLayout = new RelativeLayout(this);
		titleLayout.setId(titleLayoutId);
		titleLayout.setBackgroundColor(bgColor);
		RelativeLayout.LayoutParams titleLayoutLp = new RelativeLayout.LayoutParams(
				LayoutParams.MATCH_PARENT, titleLayoutHeight);
		titleLayout.setLayoutParams(titleLayoutLp);

		// Title
		int titleHeight = (int) Math.ceil(0.08 * width * logicalDensity);
		ImageView title = new ImageView(this);
		title.setImageResource(R.drawable.foodr);
		RelativeLayout.LayoutParams titleLp = new RelativeLayout.LayoutParams(
				LayoutParams.WRAP_CONTENT, titleHeight);
		titleLp.addRule(RelativeLayout.ALIGN_LEFT);
		titleLp.addRule(RelativeLayout.CENTER_IN_PARENT);
		titleLp.leftMargin = (int) Math.ceil(10 * logicalDensity);
		title.setLayoutParams(titleLp);
		titleLayout.addView(title);

		// Card container
		mCardContainer = new CardContainer(this);

		int cardW = (int) Math.ceil(0.8 * width);
		int cardH = (int) Math.ceil(0.7 * height);
		RelativeLayout.LayoutParams cardLp = new RelativeLayout.LayoutParams(
				cardW, cardH);
		cardLp.addRule(RelativeLayout.CENTER_HORIZONTAL);
		cardLp.addRule(RelativeLayout.BELOW, titleLayoutId);
		cardLp.topMargin = (int) Math.ceil(0.15 * height * logicalDensity);
		mCardContainer.setLayoutParams(cardLp);

		contentView.addView(titleLayout);
		contentView.addView(mCardContainer);
		return contentView;
	}

	class DownloadImage extends AsyncTask<Object, Object, Drawable> {

		private CardModel cm;
		private URL url;
		private SimpleCardStackAdapter scsa;

		@Override
		protected Drawable doInBackground(Object... params) {
			cm = (CardModel) params[0];
			url = (URL) params[1];
			scsa = (SimpleCardStackAdapter) params[2];

			try {
				return drawableFromUrl(url);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Drawable result) {
			if (result != null) {
				cm.setCardImageDrawable(result);
				scsa.notifyDataSetChanged();
			}
		}

	}

	private void loadNextPhoto() {
		if (mPhotoLoadedIndex < restaurants.size()) {
			Restaurant r = restaurants.get(mPhotoLoadedIndex);
			CardModel cm = mAdapter.getCardModel(mPhotoLoadedIndex);
			final URL photoUrl = r.getPhotos().get(0).getUrl();

			(new DownloadImage()).execute(cm, photoUrl, mAdapter);
			mPhotoLoadedIndex++;
		}
	}

	public static Drawable drawableFromUrl(URL url) throws IOException {
		Bitmap x;

		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.connect();
		InputStream input = connection.getInputStream();

		x = BitmapFactory.decodeStream(input);
		return new BitmapDrawable(x);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.card, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}