package hack.mit.foodr;

import java.util.ArrayList;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationClient;

import model.Restaurant;
import hack.mit.foodr.util.RestaurantGetter;
import views.CircularImageView;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.Window;
import android.widget.AbsoluteLayout;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;

public class FoodrSplash extends Activity implements
		GooglePlayServicesClient.ConnectionCallbacks,
		GooglePlayServicesClient.OnConnectionFailedListener {

	// Images for the splash screen
	private int[] imageResources = { R.drawable.cheesecake,
			R.drawable.chocolatesponge, R.drawable.lamb, R.drawable.mangocake,
			R.drawable.oysters, R.drawable.pasta, R.drawable.shrimp,
			R.drawable.springrolls };

	// Initializer
	private TryLocationManager init;
	private boolean locationManagerLock;

	// Location client
	private LocationClient mLocationClient;
	private Location mCurrentLocation;

	// Check if services exist
	private boolean servicesConnected;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Set the location manager lock to false
		locationManagerLock = false;

		// Background color
		int bgColor = 0xffd25951;

		// Remove title bar
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);

		AbsoluteLayout al = new AbsoluteLayout(this);

		// Compute the display size
		Display display = getWindowManager().getDefaultDisplay();
		int width = display.getWidth(); // deprecated
		int height = display.getHeight(); // deprecated;

		// Draw the images in a circle
		int imgCounter = 0;
		double imgDimension = Math.min(width / 4, height / 4);
		double radius = (0.95 * width - imgDimension) / 2.0;
		double dAngle = 360.0 / imageResources.length;
		for (double angle = 0; angle < 360; angle += dAngle) {
			double radAngle = Math.toRadians(angle);
			double x = (Math.cos(radAngle)) * radius + width / 2 - imgDimension
					/ 2;
			double y = (Math.sin(radAngle)) * radius + height / 2
					- imgDimension / 2;
			CircularImageView img = new CircularImageView(this);
			img.setImageResource(imageResources[imgCounter]);
			AbsoluteLayout.LayoutParams lp = new AbsoluteLayout.LayoutParams(
					(int) imgDimension, (int) imgDimension, (int) x, (int) y);
			img.setLayoutParams(lp);
			al.addView(img);

			imgCounter++;
		}

		// Draw the logo
		double logoDimension = imgDimension * 1.5;
		ImageView img = new ImageView(this);
		img.setImageResource(R.drawable.foodr);
		double x = width / 2 - logoDimension / 2;
		double y = height / 2 - logoDimension / 2;
		img.setScaleType(ScaleType.CENTER_INSIDE);
		AbsoluteLayout.LayoutParams lp = new AbsoluteLayout.LayoutParams(
				(int) logoDimension, (int) logoDimension, (int) x, (int) y);
		img.setLayoutParams(lp);
		al.addView(img);

		al.setBackgroundColor(bgColor);

		setContentView(al);
		init = new TryLocationManager();
	}

	@Override
	protected void onStart() {
		super.onStart();

		// Check if google play services exist
		servicesConnected = servicesConnected();

		if (servicesConnected) {
			// Create location client
			mLocationClient = new LocationClient(this, this, this);

			// Try to connect
			mLocationClient.connect();
		} else {
			if (locationManagerLock == false) {
				locationManagerLock = true;

				// Execute second option
				init.execute();
			}
		}
	}

	@Override
	protected void onStop() {
		// Disconnect on stop
		mLocationClient.disconnect();
		super.onStop();
	}

	public class TryLocationManager extends
			AsyncTask<Integer, Integer, Integer> {

		@Override
		protected Integer doInBackground(Integer... params) {
			// Get my location
			Criteria criteria = new Criteria();
			LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
			String bestProvider = locationManager.getBestProvider(criteria,
					true);
			mCurrentLocation = locationManager
					.getLastKnownLocation(bestProvider);

			if (mCurrentLocation == null) {
				Log.v("LocationProvider", "We tried with " + bestProvider
						+ " but failed.");

				// Try with network
				mCurrentLocation = locationManager
						.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

				// Just abort
				if (mCurrentLocation == null)
					return null;
			}

			// TODO: Allow custom queries and filters.
			return null;
		}

		protected void onPostExecute(Integer result) {
			if (mCurrentLocation != null) {
				// Execute get restaurants now
				(new GetRestaurants()).execute();
			} else {
				showFailure("Error",
						"Unable to establish your location. Please check your GPS settings."
								+ " The application will now exit.");
			}
		}

	}

	public class GetRestaurants extends
			AsyncTask<Location, Integer, ArrayList<Restaurant>> {

		@Override
		protected ArrayList<Restaurant> doInBackground(Location... params) {
			ArrayList<Restaurant> restaurants = new RestaurantGetter()
					.getRestaurants(mCurrentLocation);
			return restaurants;
		}

		protected void onPostExecute(ArrayList<Restaurant> result) {
			if (result == null) {
				// It failed :(
				Log.v("Error",
						"Unable to find location services. Please make sure they're enabled.");
				showFailure("Error",
						"Unable to retrieve nearby restaurants. Please check your internet connection."
								+ " The application will now exit.");
			} else {
				Intent restaurantsIntent = new Intent(FoodrSplash.this,
						CardActivity.class);

				restaurantsIntent.putExtra(CardActivity.KEY_RESTAURANTS_ARRAY,
						result.toArray(new Restaurant[result.size()]));

				startActivity(restaurantsIntent);
				finish();
			}
		}

	}

	private boolean servicesConnected() {
		// Check that Google Play services is available
		int resultCode = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(this);
		// If Google Play services is available
		if (ConnectionResult.SUCCESS == resultCode) {
			// In debug mode, log the status
			Log.d("Location Updates", "Google Play services is available.");
			// Continue
			return true;
			// Google Play services was not available for some reason.
			// resultCode holds the error code.
		} else {
			return false;
		}
	}

	@Override
	public void onBackPressed() {
		if (init != null)
			init.cancel(true);

		super.onBackPressed();
	}

	@Override
	public void onConnectionFailed(ConnectionResult result) {
		servicesConnected = false;
		if (locationManagerLock == false) {
			locationManagerLock = true;

			// Execute second option
			init.execute();
		}
	}

	@Override
	public void onConnected(Bundle connectionHint) {
		mCurrentLocation = mLocationClient.getLastLocation();

		if (mCurrentLocation != null) {
			// Yay we have a location
			(new GetRestaurants()).execute();
		} else {
			if (locationManagerLock == false) {
				locationManagerLock = true;

				// Execute second option
				init.execute();
			}
		}
	}

	@Override
	public void onDisconnected() {
		// TODO Auto-generated method stub

	}

	private void showFailure(String title, String msg) {
		new AlertDialog.Builder(this)
				.setTitle(title)
				.setMessage(msg)
				.setPositiveButton("Okay",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								finish();
							}
						}).setIcon(android.R.drawable.ic_dialog_alert)
				.setCancelable(false).show();
	}
}
