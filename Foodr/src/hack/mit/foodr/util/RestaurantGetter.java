package hack.mit.foodr.util;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;

import model.Photo;
import model.Restaurant;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.scribe.builder.ServiceBuilder;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.model.Verb;
import org.scribe.oauth.OAuthService;

import android.location.Location;
import android.util.Log;

/**
 * Code sample for accessing the Yelp API V2.
 * 
 * This program demonstrates the capability of the Yelp API version 2.0 by using
 * the Search API to query for businesses by a search term and location, and the
 * Business API to query additional information about the top result from the
 * search query.
 * 
 * <p>
 * See <a href="http://www.yelp.com/developers/documentation">Yelp
 * Documentation</a> for more info.
 * 
 */
public class RestaurantGetter {

	private static final String API_HOST = "api.yelp.com";
	private static final String DEFAULT_TERM = "restaurant";
	private static final String DEFAULT_LOCATION = "Cambridge, MA";
	private static final int SEARCH_LIMIT = 100;
	private static final String SEARCH_PATH = "/v2/search";
	private static final String BUSINESS_PATH = "/v2/business";

	/*
	 * Update OAuth credentials below from the Yelp Developers API site:
	 * http://www.yelp.com/developers/getting_started/api_access
	 */
	private static final String CONSUMER_KEY = "SlMvJuOQywTNrdS21NJ2Ow";
	private static final String CONSUMER_SECRET = "GoR40aQaBmri6bmmlJWQig4Oezs";
	private static final String TOKEN = "_rlDPlWrsysxI3jB0mSw1AaR0aVXqa7s";
	private static final String TOKEN_SECRET = "R1lwbOH2uR6mrunongmA2y03HaE";

	private static ArrayList<Restaurant> restaurants;

	OAuthService service;
	Token accessToken;

	/**
	 * Setup the Yelp API OAuth credentials.
	 * 
	 * @param consumerKey
	 *            Consumer key
	 * @param consumerSecret
	 *            Consumer secret
	 * @param token
	 *            Token
	 * @param tokenSecret
	 *            Token secret
	 */
	public RestaurantGetter() {
		this.service = new ServiceBuilder().provider(TwoStepOAuth.class)
				.apiKey(CONSUMER_KEY).apiSecret(CONSUMER_SECRET).build();
		this.accessToken = new Token(TOKEN, TOKEN_SECRET);
	}

	/**
	 * Creates and sends a request to the Search API by term and location.
	 * <p>
	 * See <a
	 * href="http://www.yelp.com/developers/documentation/v2/search_api">Yelp
	 * Search API V2</a> for more info.
	 * 
	 * @param term
	 *            <tt>String</tt> of the search term to be queried
	 * @param location
	 *            <tt>String</tt> of the location
	 * @return <tt>String</tt> JSON Response
	 */
	public String searchForBusinessesByLocation(Location l, int i) {
		// TODO: Use custom queries instead of hardcoding.
		OAuthRequest request = createOAuthRequest(SEARCH_PATH);
		request.addQuerystringParameter("term", "food");
		request.addQuerystringParameter("ll",
				"" + l.getLatitude() + "," + l.getLongitude());
		request.addQuerystringParameter("limit", "20");
		request.addQuerystringParameter("offset", "" + (20 * i));
		return sendRequestAndGetResponse(request);
	}

	/**
	 * Creates and sends a request to the Business API by business ID.
	 * <p>
	 * See <a
	 * href="http://www.yelp.com/developers/documentation/v2/business">Yelp
	 * Business API V2</a> for more info.
	 * 
	 * @param businessID
	 *            <tt>String</tt> business ID of the requested business
	 * @return <tt>String</tt> JSON Response
	 */
	public String searchByBusinessId(String businessID) {
		OAuthRequest request = createOAuthRequest(BUSINESS_PATH + "/"
				+ businessID);
		return sendRequestAndGetResponse(request);
	}

	/**
	 * Creates and returns an {@link OAuthRequest} based on the API endpoint
	 * specified.
	 * 
	 * @param path
	 *            API endpoint to be queried
	 * @return <tt>OAuthRequest</tt>
	 */
	private OAuthRequest createOAuthRequest(String path) {
		OAuthRequest request = new OAuthRequest(Verb.GET, "http://" + API_HOST
				+ path);
		return request;
	}

	/**
	 * Sends an {@link OAuthRequest} and returns the {@link Response} body.
	 * 
	 * @param request
	 *            {@link OAuthRequest} corresponding to the API request
	 * @return <tt>String</tt> body of API response
	 */
	private String sendRequestAndGetResponse(OAuthRequest request) {
		System.out.println("Querying " + request.getCompleteUrl() + " ...");
		this.service.signRequest(this.accessToken, request);
		Response response = request.send();
		return response.getBody();
	}

	/**
	 * Queries the Search API based on the command line arguments and takes the
	 * first result to query the Business API.
	 * 
	 * @param yelpApi
	 *            <tt>YelpAPI</tt> service instance
	 * @param yelpApiCli
	 *            <tt>YelpAPICLI</tt> command line arguments
	 */
	public ArrayList<Restaurant> getRestaurants(Location deviceLocation) {
		ArrayList<Restaurant> restaurants = new ArrayList<Restaurant>();
		for (int i = 0; i < 10; i++) {
			String searchResponseJSON = searchForBusinessesByLocation(
					deviceLocation, i);
			JSONParser parser = new JSONParser();
			JSONObject response = null;
			try {
				response = (JSONObject) parser.parse(searchResponseJSON);
			} catch (ParseException pe) {
				System.out.println("Error: could not parse JSON response:");
				System.out.println(searchResponseJSON);
				System.exit(1);
			}

			System.out.println(response.toJSONString());
			JSONArray businesses = (JSONArray) response.get("businesses");
			for (int j = 0; j < businesses.size(); j++) {
				JSONObject business = (JSONObject) businesses.get(j);
				JSONObject location = (JSONObject) business.get("location");
				String id = business.get("id").toString();
				String address = location.get("address").toString();
				double distance = Double.parseDouble(business.get("distance")
						.toString());
				String phone = (business.get("display_phone") != null) ? business
						.get("display_phone").toString() : "";
				String name = business.get("name").toString();
				String yelpUrl = business.get("url").toString();
				String url = yelpUrl;

				/**
				 * try { Document doc =
				 * Jsoup.connect(url).userAgent("Mozilla").get(); Elements
				 * summary = doc.select(".island.summary"); if
				 * (summary.first().children().select(".nowrap.extra.open")
				 * .size() == 0) { // we're closed :'( continue; } } catch
				 * (Exception e) { e.printStackTrace(); }
				 **/

				Restaurant r = new Restaurant(id, name, address, distance,
						null, phone, yelpUrl);
				restaurants.add(r);
				Photo.updateRestaurantPhotos(r);
			}
		}
		for (Restaurant r : restaurants) {
			System.out.println(r);
		}
		return restaurants;
	}
}
