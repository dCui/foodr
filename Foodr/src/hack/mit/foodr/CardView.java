package hack.mit.foodr;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.View;

/**
 * TODO: document your custom view class.
 */
public class CardView extends View {

	public CardView(Context context) {
		super(context);
	}
	
    public CardView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
}
